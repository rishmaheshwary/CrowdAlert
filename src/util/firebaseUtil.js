import { firebaseConfig } from "../config";
import * as fbase from "firebase";
const firebase = fbase.initializeApp(firebaseConfig);
function snapshotToArray(snapshot) {
    var returnArr = [];
    snapshot.forEach(function(childSnapshot) {
        var item = {}
	item=childSnapshot.val();
        var k = childSnapshot.key;
        var flag=0;
        console.log(item);
        console.log(k);
	for (var k1 in item)
	{	
		if(item.hasOwnProperty(k1)){
			if(k1=="deleted"&&item["deleted"]>0){
				flag=1;
			}
		}	
	}
	if (flag==0){
	returnArr.push({ key : k , value :item });
        }
    });
    return returnArr;
};
// Adds users data to the 'users' collection
addUser = async userDetails => {
  const userKey = userDetails.email.replace(".", "");
  var userRef = firebase.database().ref("users/" + userKey);
  userRef.on("value", function(snapshot) {
    if (snapshot.exists()) {
      console.log("User Exists");
    } else {
      // Adding User to 'users' collection
      firebase
        .database()
        .ref("users")
        .child(userKey)
        .set(userDetails)
        .catch(error => console.log(error));
    }
  });
};

// Queries all the incidents posted by a user
// user_id is defined by the email id
query = async user_id => {
  console.log("input", user_id);
  const user_key = user_id.replace(".", "");
  return new Promise((resolve, reject) => {
    var incident_ref = firebase.database().ref("incidents/");
    incident_ref
      .orderByChild("user_id")
      .equalTo(user_key)
      .on("value", function(snapshot) {
	console.log("######################################################################")      
	 //console.log(snapshot);
        var items = [].concat(snapshotToArray(snapshot));        
        console.log(items);
	resolve(items);
      });
  });
};

// Given an incident_id it increments the report report_count
// TODO: Get reason from the user who reported it
report = async incident_id => {
  databaseRef = firebase
    .database()
    .ref("incidents")
    .child(incident_id)
    .child("report_count");
  await databaseRef
    .transaction(function(report_count) {
      return report_count + 1;
    })
    .then(result => {
      console.log("reported", result);
      alert("Incident has been reported");
    })
    .catch(error => {
      console.log(error);
      alert("Some error occured: ", error)
    });
};
report1 = async incident_id => {
  databaseRef = firebase
    .database()
    .ref("incidents")
    .child(incident_id)
    .child("deleted");
  await databaseRef
    .transaction(function(deleted) {
         return deleted + 1;
    })
    .then(result => {
      console.log("reported", result);
      alert("Incident has been deleted");
    })
    .catch(error => {
      console.log(error);
      alert("Some error occured: ", error)
    });
};
export { report, addUser, query, firebase };